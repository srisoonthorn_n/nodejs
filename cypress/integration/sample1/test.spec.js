/// <reference types="Cypress" />
Cypress.on('uncaught:exception', (err, runna) => {
    //returning false here prevent Cypress from
    //failing the test
    return false
})
describe('Fill the form test', () => {
    it('calmoney', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')

        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetTHB').click()
        cy.get('#sourceAmount')
            .type('30')
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', '900.00 THB')
            .and('have.class', 'alert alert-success')

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetTHB').click()
        cy.get('#sourceAmount')
            .type('-1')
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', 'Input positive number')
            .and('have.class', 'alert alert-success')

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetTHB').click()
        cy.get('#sourceAmount')
            .type('a')
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', 'Input Number!!!')
            .and('have.class', 'alert alert-success')

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetTHB').click()
        cy.get('#sourceAmount')
            .type('2.05')
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', '61.50 THB')
            .and('have.class', 'alert alert-success')

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetTHB').click()
        cy.get('#sourceAmount')
            .type('0')
        cy.get('#calBtn').click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', '0 THB')
            .and('have.class', 'alert alert-success')



    })
})
