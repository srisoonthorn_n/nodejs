/// <reference types="Cypress" />
Cypress.on('uncaught:exception', (err, runna) => {
    //returning false here prevent Cypress from
    //failing the test
    return false
})
describe('Fill the form test', () => {
    // it('open the registra and search', () => {
    //     cy.visit('https://reg.cmu.ac.th')
    //     cy.get('[href=" https://www1.reg.cmu.ac.th/web/th/student/"] > img')
    //         .click()
    //     cy.get('.panel-body > [href="https://www1.reg.cmu.ac.th/registrationoffice/searchcourse.php"]')
    //         .click()
    //     cy.get('#fgroup')
    //         .type('953')
    //     cy.get('#button')
    //         .click()
    // })
    // it('open the registra and search', () => {
    //     cy.visit('https://itsc.cmu.ac.th/')
    //     cy.get('.modal-footer > .btn').click()
    //     cy.get('#h6-mega-dropdown').click()
    //     cy.get('#wucNavBar_wucNavbarOnlineStoreage_rptNav_hlLink_1').click()
    // })
    it('calmoney', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')

        cy.get('#selectSource > .caret').click()
        cy.get('#sourceTHB').click()
        cy.get('#sourceAmount')
            .type('100')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetUSD').click()
        cy.get('#sourceAmount').click()
            .type('300')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetEUR').click()
        cy.get('#sourceAmount').click()
            .type('400')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#sourceAmount')
            .type('500')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetUSD').click()
        cy.get('#sourceAmount')
            .type('600')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetEUR').click()
        cy.get('#sourceAmount')
            .type('700')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceEUR').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetTHB').click()
        cy.get('#sourceAmount')
            .type('800')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceEUR').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetUSD').click()
        cy.get('#sourceAmount')
            .type('900')
        cy.get('#calBtn').click()

        cy.reload()
        cy.get('#selectSource > .caret').click()
        cy.get('#sourceEUR').click()
        cy.get('#selectTarget > .caret').click()
        cy.get('#targetEUR').click()
        cy.get('#sourceAmount')
            .type('1000')
        cy.get('#calBtn').click()


    })
})
